# 30 Day Map Challenge 2022
# Day 8 - Open Street Map data 
# Jonas Stillhard, October 2022
# 
# This script creates the map for the 30DayMapChallenge for October 8th 
# It is based on data from open street map. 
# The vignette of the "osmdata" package provides examples on 
# how to retrieve data from OSM
# https://cran.r-project.org/web/packages/osmdata/vignettes/osmdata.html
# 
# 
# This script requires the following packages: 
# - osmdata: load data from the osm api
# - sf: Spatial features


# Fetch bounding box
# The osmdata package requires a bounding box which can then be 
# used to retrieve additional data
# To get data for the city of Zurich, specify the "Bezirk Zürich"
bb_zurich <- osmdata::getbb(place_name = "Zürich"
                            , format_out = "data.frame")

w_dir <- "C:/Users/stillhard/Documents/R/30DayMapChallenge_2022/day_8_osm"


bb_zurich$display_name

bb_zurich <- matrix(c(8.507, 8.524
                    , 47.370, 47.384), nrow = 2)

osmdata::available_tags("water")

# Query the data
# the opq function retrieves a query which can then be sent to the
# api
all_roads <- osmdata::opq(bb_zurich) |> 
  osmdata::add_osm_feature(key = 'highway'
                           # , value = c("bridleway"
                           #             , "motorway"
                           #             , "trunk"
                           #             , "primary"
                           #             , "secondary"
                           #             , "tertiary"
                           #             , "motorway_link"
                           #             , "trunk_link"
                           #             , "secondary_link"
                           #             , "tertiary_link"
                           #             , "pedestrian"
                           #             , "cycleway"
                           #             , "footway"
                           #             , "living_strees")
                           ) |> 
  osmdata::osmdata_sf() |>  
  base::`$`(osm_lines) |>  
  sf::st_as_sf()

rw <- osmdata::opq(bb_zurich) |>
  osmdata::add_osm_feature(key = "railway:track_ref") |>
  osmdata::osmdata_sf() |>
  base::`$`(osm_lines) |>
  sf::st_as_sf() |>
  sf::st_crop(xmin = bb_zurich[1, 1]
              , xmax = bb_zurich[2, 1]
              , ymin = bb_zurich[1, 2]
              , ymax = bb_zurich[2, 2])

bldngs <- osmdata::opq(bb_zurich) |>
  osmdata::add_osm_feature(key = "building") |>
  osmdata::osmdata_sf() |> 
  base::`$`(osm_polygons) |> 
  sf::st_as_sf() |>
  sf::st_crop(xmin = bb_zurich[1, 1]
              , xmax = bb_zurich[2, 1]
              , ymin = bb_zurich[1, 2]
              , ymax = bb_zurich[2, 2])





# Some of the geometries go beyond the bbox
all_roads <- sf::st_crop(all_roads
                         , xmin = bb_zurich[1, 1]
                         , xmax = bb_zurich[2, 1]
                         , ymin = bb_zurich[1, 2]
                         , ymax = bb_zurich[2, 2]
)


png(paste0(w_dir, "/day_8.png"), width = 2500, height = 2500, res = 200)

        par(bg = "darkgoldenrod"
            , mar = rep(0, 4)
            , oma = rep(0, 4)
            )
        plot(0
             , type = "n"
             , xlim = c(bb_zurich[1,1]+0.005, bb_zurich[2, 1]-0.009)
             , ylim = c(bb_zurich[1, 2]+0.001, bb_zurich[2, 2] - 0.001)
             , axes = F
             , bty = "n"
             , xlab = ""
             , ylab = ""
             # , col = "grey89"
             , asp = 1
             )
        
        
        plot(sf::st_geometry(all_roads)
             , col = "grey75"
             , add = T)
        
        plot(sf::st_geometry(rw)
             , add = T
             , col = "grey78")
        
        plot(sf::st_geometry(bldngs)
             , add = T
             , col = "grey80"
             , border = "grey80"
        )
        
        # text("topright"
        #      , "Aussersihl / Wiedikon"
        #      , col = "grey50")

dev.off()

