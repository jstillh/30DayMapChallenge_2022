# 30DayMapChallenge 2022
*Jonas Stillhard, October 2022*  

This repository contains code and data used for the 30 Day Map Challenge in November 2022. I use openly available data for the maps
They are split into a single folder for each day: 

- [Day 1: Points](./day_1_points): Based on data from [Mauri et al. 2017:](https://www.nature.com/articles/sdata2016123) EU-Trees4F, a dataset on the future distribution of European tree species.   
- [Day 2: Lines](./day_2_lines): Based on data from [our data paper](https://esajournals.onlinelibrary.wiley.com/doi/full/10.1002/ecy.2845): 
Stand inventory data from the 10-ha forest research plot in Uholka: 15 yr of primeval beech forest development. Joyplot based on the DTM. 
- [Day 3: Polygons](./day_3_polygons): Again based on [Mauri et al. 2017:](https://doi.org/10.1038/sdata.2016.123). Bounding boxes of all *Quercus* species within the dataset.
- [Day 4: Colour Friday: Green](./day_4_green): All appearances of the word "Green" in German, French and Italian in the swissNAMES3D dataset of swisstopo. 
- [Day 5: Ukraine](./day_5_ukraine): All locations where the GBIF database contain _Helianthus anuus_ in Ukraine   
- [Day 6: Network](./day_6_network): Commuting in Switzerland, based on data from the Swiss Federal Office for statistics 
- [Day 7: Raster](./day_7_raster): A hillshade of Germany, based on SRTM Data

- [Day 8: Data: OSM](./day_8_osm): Just a nice little map based on OSM data from my neighborhood.
- [Day 9: Space](./day_9_space): UFO-sightings worldwide.
- [Day 10: A bad map](./day_10_bad): Michael Jacksons "Bad" Tour.

- [Day 11: Colour Friday: Red](./day_11_red)
- [Day 12: Scale](./day_12_scale)  
- [Day 13: 5 minute map](./day_13_5min)  
- [Day 14: Hexagons](./day_14_hex)  
- [Day 15: Food / Drink](./day_15_food)  
- [Day 16: Minimal](./day_16_minimal)  
- [Day 17: w/o computer](./day_17_nocomp)  
- [Day 18: Colour Friday: Blue](./day_18_blue)  
- [Day 19: Globe](./day_19_globe)  
- [Day 20: My favourite](./day_20_favourite)  
- [Day 21: Kontur Population Dataset](./day_21_kontur)
<!---
- [Day 22: *NULL*](./day_22_null)  
- [Day 23: Movement](./day_23_movement)  
- [Day 24: Fantasy](./day_24_fantasy)  
- [Day 25: Colour Friday: 2 Colors](./day_25_2cols)  
- [Day 26: Islands](./day_26_islands)  
- [Day 27: Music](./day_27_music)  
- [Day 28: 3D](./day_28_3d)  
- [Day 29: out of my comfort zone](./day_29_uncomfy)  
- [Day 30: Remix](./day_30_remix)   


## datasets to check out

#### [Kindermann et al. 2018:](https://www.nature.com/articles/sdata201877): 109 years of forest growth measurements from individual Norway spruce trees 
Repeated measurements of planted *P. abies* plantation, including coordinates.  Maybe do something with 3D / height measurements etc. 

#### [Zhenrong Du et al. 2022:](https://www.nature.com/articles/s41597-022-01260-2) A global map of planting years of plantations 

####  [Mauri et al. 2022:](https://www.nature.com/articles/s41597-022-01128-5) EU-Trees4F, a dataset on the future distribution of European tree species  

####  `data(trees, package = "datasets")`  

####  [Allan et al. 2017:](https://www.nature.com/articles/sdata2017187) Temporally inter-comparable maps of terrestrial wilderness and the Last of the Wild  

####  [Lesiv et al. 2022:](https://www.nature.com/articles/s41597-022-01332-3) Global forest management data for 2015 at a 100 m resolution

#### [Woods 2009:](https://esajournals.onlinelibrary.wiley.com/doi/10.1890/09-0565.1) Multi-decade, spatially explicit population studies of canopy dynamics in Michigan old-growth forests.

#### [Pélissier et al. 2007:](https://esajournals.onlinelibrary.wiley.com/doi/10.1890/10-1991.1) Tree demography in an undisturbed Dipterocarp permanent sample plot at Uppangala, Western Ghats of India  
Data with coordinates, collected on transects of a total of 3.2 ha.

#### [Ju et al. 2022:](https://www.nature.com/articles/s41597-022-01701-y) A 10 m resolution urban green space map for major Latin American cities from Sentinel-2 remote sensing images and OpenStreetMap   
For Day 4, Color Friday: Green

#### [Cagnacci et al. 2016](https://datadryad.org/stash/dataset/doi:10.5061/dryad.rg0v3)
-->


